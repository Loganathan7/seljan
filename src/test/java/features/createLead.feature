Feature: Creating a New Lead with Basic information

#Background:
#    Given Open the browser
#    And Load the URL

@smoke @reg
Scenario Outline: Postive WorkFlow 1
    And Enter the UserName as <UserName>
    And Enter the Password as <Password>
    And Click on the Login
    And Click on the CRMSFA
    And Click on the Leads Tab
    And Click on the Create Lead shortcut
    And Enter the Company Name as <CName>
    And Enter the First Name as <FName>
    And Enter the Last Name as <LName>
    When Click on Create Lead
    Then verify the Lead is created as <LeadFName>
  
  Examples:
  |UserName|Password|CName|FName|LName|LeadFName|
  |DemoCSR|crmsfa|CEIlmt|Keerthi|Logan|Keerthi|
  |DemoSalesManager|crmsfa|CEIlmt|Keerthiga|Loganathan|Keerthiga|
  
    @sanity
    Scenario: Postive WorkFlow 2
    And Enter the UserName as DemoSalesManager
    And Enter the Password as crmsfa
    And Click on the Login
    And Click on the CRMSFA
    And Click on the Leads Tab
    And Click on the Create Lead shortcut
    And Enter the Company Name as Accenture
    And Enter the First Name as KK
    And Enter the Last Name as Logan
    When Click on Create Lead
    Then verify the Lead is created as KK

#Scenario: Postive WorkFlow 1
#    Given Open the browser
#    And Load the URL
#    And Enter the UserName as DemoCSR
#    And Enter the Password as crmsfa
#    And Click on the Login
#    And Click on the CRM/SFA
#    And Click on the Leads Tab
#    And Click on the Create Lead shortcut
#    And Enter the Company Name
#    And Enter the First Name
#    And Enter the Last Name
#    When Click on Create Lead
#    Then verify the Lead is created
#    
#    Scenario: Postive WorkFlow 2
#    Given Open the browser
#    And Load the URL
#    And Enter the UserName as DemoSalesManager
#    And Enter the Password as crmsfa
#    And Click on the Login
#    And Click on the CRM/SFA
#    And Click on the Leads Tab
#    And Click on the Create Lead shortcut
#    And Enter the Company Name
#    And Enter the First Name
#    And Enter the Last Name
#    When Click on Create Lead
#    Then verify the Lead is created