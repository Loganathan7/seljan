package steps;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase {
	
	@Before
	public void beforeMethod(Scenario sc) {
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true); 
		extent   = new ExtentReports();
		extent.attachReporter(reporter);
		test = extent.createTest(sc.getName(), sc.getId());
	    test.assignAuthor("Keerthiga");
	    test.assignCategory("smoke");
	    startApp("chrome", "http://leaftaps.com/opentaps");		
				
	}

	@After
	public void afterMethod(Scenario sc) {
		System.out.println(sc.getStatus());
		close(); /*Method written in Selenium Base Class*/
		stopReport(); /*Method written in Reporter Class*/

		
		
		
		
//	@Before
//	public void beforeMethod(Scenario sc) {
//		System.out.println(sc.getName());
//		System.out.println(sc.getId());
//		//		System.out.println(sc.getStatus());
//	}
//
//	@After
//	public void afterMethod(Scenario sc) {
//		System.out.println(sc.getStatus());

	}

}
