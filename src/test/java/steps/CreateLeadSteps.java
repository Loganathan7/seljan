//package steps;
//
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.RemoteWebDriver;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class CreateLeadSteps {
//
//	RemoteWebDriver driver;
//
//	@Given("Open the browser")
//	public void openTheBrowser() {
//		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//	}
//
//	@And("Load the URL")
//	public void loadTheURL() {
//		driver.get("http://leaftaps.com/opentaps/control/main");
//	}
//
//	@And("Enter the UserName as (.*)")
//	public void enterTheUserName(String Uname) {
//		driver.findElementById("username").sendKeys(Uname);
//	}
//
//	@And("Enter the Password as (.*)")
//	public void enterThePassword(String pwd) {
//		driver.findElementById("password").sendKeys(pwd);
//	}
//
//	@And("Click on the Login")
//	public void clickOnTheLogin() {
//		driver.findElementByClassName("decorativeSubmit").click();
//	}
//
//	@And("Click on the CRM/SFA")
//	public void clickOnTheCRMSFA() {
//		driver.findElementByXPath("//div[@class=\'crmsfa\']/a").click();
//	}
//
//	@And("Click on the Leads Tab")
//	public void clickOnTheLeadsTab() {
//		driver.findElementByLinkText("Leads").click();
//	}
//
//	@And("Click on the Create Lead shortcut")
//	public void clickOnTheCreateLeadShortcut() {
//		driver.findElementByLinkText("Create Lead").click();
//	}
//
//	@And("Enter the Company Name as (.*)")
//	public void enterTheCompanyName(String Cname) {
//		driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
//	}
//
//	@And("Enter the First Name as (.*)")
//	public void enterTheFirstName(String Fname) {
//		driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
//	}
//
//	@And("Enter the Last Name as (.*)")
//	public void enterTheLastName(String Lname) {
//		driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
//	}
//
//	@When("Click on Create Lead")
//	public void clickOnCreateLead() {
//		driver.findElementByClassName("smallSubmit").click();
//	}
//
//	@Then("verify the Lead is created")
//	public void verifyTheLeadIsCreated() {
//		WebElement Fname = driver.findElementById("viewLead_firstName_sp");
//		System.out.println(Fname.getText());
//	}
//
//
//}
//
