package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\java\\features", glue = {"com.yalla.pages","steps"} , monochrome = true, /*tags = "~@reg",*/ dryRun = false, snippets = SnippetType.CAMELCASE)
public class CreateLeadPOMwithCB {

}