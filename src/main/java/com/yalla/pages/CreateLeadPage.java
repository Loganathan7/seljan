package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
		
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement eleCreateLead;
	
	@And("Enter the First Name as (.*)")
	public CreateLeadPage enterFirstName(String firtsName) {
		clearAndType(eleFirstName, firtsName);
		return this;
	}
	
	@And("Enter the Last Name as (.*)")
	public CreateLeadPage enterLastName(String lastName) {
		clearAndType(eleLastName, lastName);
		return this;
	}
	
	@And("Enter the Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String companyName) {
		clearAndType(eleCompanyName, companyName);
		return this;
	}
	
	@When("Click on Create Lead")
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new ViewLeadPage();
	}

}
