package com.yalla.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class Acme {
	
@Test
public void fetchVendorName () throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		RemoteWebDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		
		driver.findElementById("email").sendKeys("devashine4u@gmail.com");
		driver.findElementById("password").sendKeys("@JUDE2527@");
		driver.findElementById("buttonLogin").click();
		
		Thread.sleep(2000);
		WebElement Vendors = driver.findElementByXPath("//button[text()=' Vendors']");
		Actions obj = new Actions(driver);
		obj.moveToElement(Vendors).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("DE987564");
		driver.findElementById("buttonSearch").click();
		
		WebElement vendorName = driver.findElementByXPath("//th[text()='Vendor']/../following-sibling::tr/td");
		System.out.println("Vendor Name for the entered Tax ID is: "+vendorName.getText());
		driver.close();
	}
}
