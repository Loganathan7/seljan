package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {
	
	@BeforeTest
	public void testCaseDetails() {
		testcaseName = "TC002_Run create Lead page";
		testcaseDec = "Creating a new Lead and verifying the First Name";
		author = "Keerthiga";
		category = "smoke";
		excelFileName = "TC001";
//		excelFileName = "TC001_MultipleTabs";
		excelsheetName1 = "login";
//		excelsheetName2 = "userDetails";
	}
	
	@Test(dataProvider="fetchData") 
	public void createLeadTC(String uName, String pwd, String fName, String lName, String cName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRMlink()
		.clickLeads()
		.clickCreateLead()
		.enterFirstName(fName)
		.enterLastName(lName)
		.enterCompanyName(cName)
		.clickCreateLead()
		.verifyFirstName("Keer");	
		
	}

}
