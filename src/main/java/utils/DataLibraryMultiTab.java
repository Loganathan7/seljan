package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataLibraryMultiTab {

	public static Object[][] readExcelData(String excelfileName, String sheet1, String sheet2) throws IOException {
		XSSFWorkbook wbook = 
				new XSSFWorkbook("./data/"+excelfileName+".xlsx");
		XSSFSheet sheetone = wbook.getSheet(sheet1);
		int rowCountsheet1 = sheetone.getLastRowNum();
		System.out.println("Row Count is: "+rowCountsheet1);
		int colCountsheet1 = sheetone.getRow(0).getLastCellNum();
		System.out.println("Col Count is: "+colCountsheet1);
		Object[][] data = new Object[rowCountsheet1][colCountsheet1];

		for (int i = 1; i <= rowCountsheet1; i++) {
			XSSFRow row = sheetone.getRow(i);
			for (int j = 0; j < colCountsheet1; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				data[i-1][j] = stringCellValue;
				System.out.println(stringCellValue);
				if (i > rowCountsheet1 && j > colCountsheet1) {
					XSSFSheet sheetTwo = wbook.getSheet(sheet2);
					int rowCountsheet2 = sheetTwo.getLastRowNum();
					System.out.println("Row Count is: "+rowCountsheet2);
					int colCountsheet2 = sheetTwo.getRow(0).getLastCellNum();
					System.out.println("Col Count is: "+colCountsheet2);
					Object[][] data2 = new Object[rowCountsheet2][colCountsheet2];
					for (int k = 1; k <= rowCountsheet2; k++) {
						XSSFRow row2 = sheetTwo.getRow(k);
						for (int l = 0; l < colCountsheet2; l++) {
							XSSFCell cell2 = row2.getCell(l);
							String stringCellValue2 = cell2.getStringCellValue();
							data[k-1][l] = stringCellValue2;
							System.out.println(stringCellValue2);
						}

					}return data2;
				}
				
			}
		}
		return data;
		
	}
	
}





