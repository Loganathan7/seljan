package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataLibrary {

	public static Object[][] readExcelData(String excelfileName, String sheet1) throws IOException {
		XSSFWorkbook wbook = 
				new XSSFWorkbook("./data/"+excelfileName+".xlsx");
		XSSFSheet sheetone = wbook.getSheet(sheet1);
		int rowCountsheet1 = sheetone.getLastRowNum();
		System.out.println("Row Count is: "+rowCountsheet1);
		int colCountsheet1 = sheetone.getRow(0).getLastCellNum();
		System.out.println("Col Count is: "+colCountsheet1);
		Object[][] data = new Object[rowCountsheet1][colCountsheet1];

		for (int i = 1; i <= rowCountsheet1; i++) {
			XSSFRow row = sheetone.getRow(i);
			for (int j = 0; j < colCountsheet1; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				data[i-1][j] = stringCellValue;
				System.out.println(stringCellValue);
				
				}
				
			}
		return data;
		
	}
	
}





